from abc import ABC, abstractclassmethod

class Animal(ABC):
	@abstractclassmethod
	def eat(self, food):
		pass

	def make_sound():
		pass

class Dog(Animal):
	def __init__(self, name, breed, age):
		super().__init__()
		self._name = name
		self._breed = breed
		self._age = age
		self._sound = "Bark! Woof! Arf!"

	#getter
	def make_sound(self):
		print(self._sound)

	# setter
	def eat(self, food):
		self._food = food
		print(f"Eaten {self._food}")

	# call
	def __call__(self):
		print(f"Here {self._name}!")

class Cat(Animal):
	def __init__(self, name, breed, age):
		super().__init__()
		self._name = name
		self._breed = breed
		self._age = age
		self._sound = "Miaow! Nyaw! Nyaaaaa!"

	#getter
	def make_sound(self):
		print(self._sound)

	# setter
	def eat(self, food):
		self._food = food
		print(f"Serve me {self._food}")

	# call
	def __call__(self):
		print(f"{self._name}, come on!")


dog1 = Dog("Isis", "Dalmatian", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1()

cat1 = Cat("Puss", "Persian", 4)
cat1.eat("Tuna")
cat1.make_sound()
cat1()